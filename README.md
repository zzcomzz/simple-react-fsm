# simple-react-fsm

> Made with create-react-library

[![NPM](https://img.shields.io/npm/v/simple-react-fsm.svg)](https://www.npmjs.com/package/simple-react-fsm) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save simple-react-fsm
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'simple-react-fsm'
import 'simple-react-fsm/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [zzCOMzz](https://github.com/zzCOMzz)
