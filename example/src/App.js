import React from 'react'

import { ExampleComponent } from 'simple-react-fsm'
import 'simple-react-fsm/dist/index.css'

const App = () => {
  return <ExampleComponent text="Create React Library Example 😄" />
}

export default App
